package com.gopi.test.testselenium;

public class ReturnTypes {

	
	public int add() {
		int a = 10;
		int b = 60;
		int c =a+b;
		System.out.println("The value of C is"+c);
		return c;	
		
	}
	
	public static void main(String[] args) {
		
		ReturnTypes rt = new ReturnTypes();
		if(rt.add()<40) {
			System.out.println("The value of C is lessthan 40");
		}
		else {
			System.out.println("The value of C is greater than or equal to 40");
		}
	}
}
